$(document).ready(function() {
    $('.js-search').click(function() {
        $(this).hide();
        $('.js-menuBlock').hide();
        $('.js-input').show();
        $('.js-input').focus();
    });

    $('.js-close').click(function() {
        $('.js-search').show();
        $('.js-menuBlock').show();
        $('.js-input').hide();
    });
});

$(".lingleo").slick({
    infinite: false,
    autoplay: true,
    dots: true,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                infinite: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                dots: true
            }
        },
        {
            breakpoint: 300,
            settings: "unslick"
        }
    ]
});